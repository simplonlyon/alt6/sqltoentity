package co.simplon.alt6.sqltoentity.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeptEmp {
    
    @ManyToOne
    @Id
    @JoinColumn(name= "emp_no",columnDefinition = "INT")
    private Employee employee;
    @ManyToOne
    @Id
    @JoinColumn(name= "dept_no", columnDefinition = "CHAR(4)")
    private Department department;
    private LocalDate fromDate;
    private LocalDate toDate;
}
