package co.simplon.alt6.sqltoentity.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="departments")
public class Department {

    @Id
    @Column(columnDefinition = "CHAR(4)")
    private String deptNo;
    @NonNull
    @Column(name="dept_name")
    private String name;

    @OneToMany(mappedBy = "department")
    private List<DeptEmp> deptEmp = new ArrayList<>();
}
