package co.simplon.alt6.sqltoentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqltoentityApplication {

	public static void main(String[] args) {		
		SpringApplication.run(SqltoentityApplication.class, args);
	}

}
