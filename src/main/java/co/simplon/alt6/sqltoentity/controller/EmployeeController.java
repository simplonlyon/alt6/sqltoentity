package co.simplon.alt6.sqltoentity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt6.sqltoentity.entity.DeptEmp;
import co.simplon.alt6.sqltoentity.entity.Employee;
import co.simplon.alt6.sqltoentity.repository.EmployeeRepository;


@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository repo;

    @GetMapping("/api/employee")
    public String getMethodName() {
        Page<Employee> emps = repo.findAll(PageRequest.of(1, 25));
        
        for (Employee employee : emps) {
            System.out.println(employee.getFirstName());
            for (DeptEmp deptEmp : employee.getDeptEmsp()) {
                System.out.println(deptEmp.getDepartment().getName());
                System.out.println(deptEmp.getFromDate());
            }
        }


        return "Yes";
    }
    
}
